'use strict';
var myApp = angular.module('App', []);
myApp.controller('MainController', function ($scope, $filter) {
    // funcion para eliminar textos vacios
    function deleteEmptySpaces(lista) {
        var i;
        for (i = 0; i < lista.length; i++) {
            if (lista[i] == " ") {
                lista.splice(i, 1);
            }
        }
        return lista;
    }
    function darvueltavector(vector) {
        var j=vector.length -1;
        var vector1= [];
        for (var i=0;i<vector.length;i++){
            vector1[i] = vector[j];
            j=j-1;
        }
        return vector1;
    }
    function createObject(lista) {
        var i, j, object, ObjectAnaliced;
        object = [];
        j = -1;
        for (i = 1; i < lista.length; i++) {
            ObjectAnaliced = lista[i].split(': ');
            if (ObjectAnaliced[0] === 'id') {
                j++;
                object[j] = {};
            }
            object[j][ObjectAnaliced[0]] = ObjectAnaliced[1];
        }
        return object;
    }
    function anadeDiasDesde(vector) {
        for (var i = 0; i < vector.length; i++) {
            var date1 = new Date(moment().format('MM/DD/YYYY hh:mm:ss'));
            var date2 = new Date(vector[i].sendDate);
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            vector[i].diasDesde = diffDays;
        };
        return vector;
    }
    //PANEL DE EMPRESA
//obtener tabla del panel de empresa
    var div, table1, table, myData;
    div = document.getElementById("solicitudTable");
    myData = div.textContent;
    var table2 = myData.split('*.....SEPARATOR.....*');
    table = table2[0].split(',,');// para tener las solicitudes que puede ver
    var tables = table2[1].split(',,');//para ver los mensajes
    var tablesa = table2[2].split(',');//para ver si la empresa esta registrada
    var tablesas = table2[3].split(',');//para poner o no el boton de cambiar el estado
    $scope.marcaseditables = [];
    table1 = deleteEmptySpaces(table);
    var tables1 = deleteEmptySpaces(tables);
    //
    $scope.loginYes = false;
    $scope.rowsPanel = createObject(table1);
    $scope.rowsPanel = _.sortBy($scope.rowsPanel, 'id');
    $scope.rowsPanel = darvueltavector($scope.rowsPanel);
    $scope.rowsPanelMessage = createObject(tables1);
    $scope.rowsPanel = anadeDiasDesde($scope.rowsPanel);
    $scope.rowslength = $scope.rowsPanel.length;
    $scope.selectedRowPanel = null;
    $scope.showApplicationResponse = false;
    $scope.shownoempresaregistered = false;
    $scope.showRowsPanel = true;
    $scope.warrantyShow = false;
    $scope.showStates = false;
    $scope.rowsPanelShowing = $scope.rowsPanel;
    $scope.rowsPanelMessageShowing = [];    //
    //functions:

    // aqui veo las marcas que se pueden editar con este usuario. No hay fallo de seguridad, esto solo se usa para que no vea
    //el boton de editar estado pero si lo cambiara el tio manualmente la variable y meta mas no podra editarlas porque
    //se hace otra comprobacion en el php
    for (var t=0;t<tablesas.length;t++) {
        if (tablesas[t].indexOf('**') > 0 && tablesas[t].substring(tablesas[t].indexOf('**'),tablesas[t].indexOf('**')-2) == ':S') {
           $scope.marcaseditables[$scope.marcaseditables.length] = tablesas[t].substring(tablesas[t].indexOf('**')-2,0);
        } else if (tablesas[t].substring(tablesas[t].length-2) == ':S' && $scope.marcaseditables[$scope.marcaseditables.length-1] != tablesas[t].substring(tablesas[t].length-2,0)) {
            $scope.marcaseditables[$scope.marcaseditables.length] = tablesas[t].substring(tablesas[t].length-2,0);
        }
    };

    $scope.selectingRowPanel = function (row) {
        $scope.selectedRowPanel = row;
        $scope.showApplicationResponse = true;
        $scope.showRowsPanel = false;
        $scope.showStates = false;
        for (var t=0;t<$scope.marcaseditables.length;t++) {
            if (row.mark.toLowerCase() == $scope.marcaseditables[t].toLowerCase()) {
                $scope.showStates = true;
            }
        }
        var j=0;
        for (var i=0; i<$scope.rowsPanelMessage.length; i++){
            if ($scope.rowsPanelMessage[i].idsolicitud == row.id){
                $scope.rowsPanelMessageShowing[j] = $scope.rowsPanelMessage[i];
                j = j+1;
            }
        }
        var entra=0;
        for (var k=0;k<tablesa.length;k++) {
            if (tablesa[k].toLowerCase()==row.mark.toLowerCase()) {
                entra = 1;
                $scope.shownoempresaregistered = false;
            } else if (entra == 0) {
                $scope.shownoempresaregistered = true;
            }
        }
        $scope.rowsPanelMessageShowing = _.sortBy($scope.rowsPanelMessageShowing, 'sendDateMessage');
        $scope.rowsPanelMessageShowing = darvueltavector($scope.rowsPanelMessageShowing);
        if ($scope.selectedRowPanel.conditions) {
            $scope.selectedRowPanel.conditions = 'Si';
        } else {
            $scope.selectedRowPanel.conditions = 'No';
        }
        $scope.selectedRowPanel.indexe = findIndexItem($scope.selectedRowPanel);
    };

    $scope.searchingdatas = function (dataname) {
        $scope.rowsPanelShowing = [];
        for (var i=0;i<$scope.rowsPanel.length;i++) {
            if (dataname.substring(0,1) == '#') {
                $scope.rowsPanel3 = '#' + $scope.rowsPanel[i].id;
            } else {
                $scope.rowsPanel3 = $scope.rowsPanel[i].id;
            }
            if ($scope.rowsPanel3.toLowerCase().indexOf(dataname.toLowerCase()) > -1 && $scope.rowsPanelShowing[$scope.rowsPanelShowing.length-1] != $scope.rowsPanel[i]) {
                $scope.rowsPanelShowing[$scope.rowsPanelShowing.length] = $scope.rowsPanel[i];
            } else if ($scope.rowsPanel[i].sendDate.toLowerCase().indexOf(dataname.toLowerCase()) > -1 && $scope.rowsPanelShowing[$scope.rowsPanelShowing.length-1] != $scope.rowsPanel[i]) {
                $scope.rowsPanelShowing[$scope.rowsPanelShowing.length] = $scope.rowsPanel[i];
            } else if ($scope.rowsPanel[i].mark.toLowerCase().indexOf(dataname.toLowerCase()) > -1 && $scope.rowsPanelShowing[$scope.rowsPanelShowing.length-1] != $scope.rowsPanel[i]) {
                $scope.rowsPanelShowing[$scope.rowsPanelShowing.length] = $scope.rowsPanel[i];
            } else if ($scope.rowsPanel[i].province.toLowerCase().indexOf(dataname.toLowerCase()) > -1 && $scope.rowsPanelShowing[$scope.rowsPanelShowing.length-1] != $scope.rowsPanel[i]) {
                $scope.rowsPanelShowing[$scope.rowsPanelShowing.length] = $scope.rowsPanel[i];
            } else if ($scope.rowsPanel[i].status.toLowerCase().indexOf(dataname.toLowerCase()) > -1 && $scope.rowsPanelShowing[$scope.rowsPanelShowing.length-1] != $scope.rowsPanel[i]) {
                $scope.rowsPanelShowing[$scope.rowsPanelShowing.length] = $scope.rowsPanel[i];
            }
        }
        if ($scope.filetId) {
            $scope.selectingIdEmpresa();
        } else if ($scope.filetMark) {
            $scope.selectingMarkEmpresa();
        } else if ($scope.filetDate) {
            $scope.selectingDateEmpresa();
        } else if ($scope.filetStatus) {
            $scope.selectingStatusEmpresa();
        } else if ($scope.filetProvince) {
            $scope.selectingProvinceEmpresa();
        }
    }

    function findIndexItem(item) {
        var i = 0;
        for (i = 0; i < $scope.rowsPanel.length; i = i + 1) {
            if (item === $scope.rowsPanel[i]) {
                return i;
            }
        }
    }

    $scope.nextItem = function () {
        var index = findIndexItem($scope.selectedRowPanel);
        if ($scope.rowsPanel[index + 1] !== undefined) {
            $scope.selectingRowPanel($scope.rowsPanel[index + 1]);
            $scope.selectedRowPanel.indexe = index + 1;
        } else {
            $scope.selectingRowPanel($scope.rowsPanel[index]);
        }
        if ($scope.selectedRowPanel.conditions) {
            $scope.selectedRowPanel.conditions = 'Si';
        } else {
            $scope.selectedRowPanel.conditions = 'No';
        }
    };

    $scope.previousItem = function () {
        var index = findIndexItem($scope.selectedRowPanel);
        if ($scope.rowsPanel[index - 1] !== undefined) {
            $scope.selectingRowPanel($scope.rowsPanel[index - 1]);
            $scope.selectedRowPanel.indexe = index - 1;
        } else {
            $scope.selectingRowPanel($scope.rowsPanel[index]);
        }
        if ($scope.selectedRowPanel.conditions === true) {
            $scope.selectedRowPanel.conditions = 'Si';
        } else if ($scope.selectedRowPanel.conditions === false) {
            $scope.selectedRowPanel.conditions = 'No';
        }
    };

    $scope.showMenuIncidences = function () {
        $scope.showApplicationResponse=false;
        $scope.showStates = false;
        $scope.shownoempresaregistered = false;
        $scope.rowsPanelMessageShowing = [];
        $scope.showRowsPanel=true;
    }
    $scope.filetMark = false;
    $scope.filetDate = false;
    $scope.filetStatus = false;
    $scope.filetProvince = false;
    $scope.filetId = false;
    $scope.selectingDateEmpresa = function () {
        $scope.filetId = false;
        $scope.filetMark = false;
        $scope.filetDate = true;
        $scope.filetStatus = false;
        $scope.filetProvince = false;
        $scope.rowsPanelShowing = _.sortBy($scope.rowsPanelShowing, 'sendDate');
    };

    $scope.selectingMarkEmpresa = function () {
        $scope.filetId = false;
        $scope.filetMark = true;
        $scope.filetDate = false;
        $scope.filetStatus = false;
        $scope.filetProvince = false;
        $scope.rowsPanelShowing = _.sortBy($scope.rowsPanelShowing, 'mark');
    };

    $scope.selectingProvinceEmpresa = function () {
        $scope.filetId = false;
        $scope.filetMark = false;
        $scope.filetDate = false;
        $scope.filetStatus = false;
        $scope.filetProvince = true;
        $scope.rowsPanelShowing = _.sortBy($scope.rowsPanelShowing, 'province');
    };
    $scope.selectingStatusEmpresa = function () {
        $scope.filetId = false;
        $scope.filetMark = false;
        $scope.filetDate = false;
        $scope.filetStatus = true;
        $scope.filetProvince = false;
        $scope.rowsPanelShowing = _.sortBy($scope.rowsPanelShowing, 'status');
    };
    $scope.selectingIdEmpresa = function () {
        $scope.filetId = true;
        $scope.filetMark = false;
        $scope.filetDate = false;
        $scope.filetStatus = false;
        $scope.filetProvince = false;
        $scope.rowsPanelShowing = _.sortBy($scope.rowsPanelShowing, 'id');
    };


    $scope.selectStatus = function(status) {
        $scope.selectedRowPanel.status = status;
        $.ajax({
                type: 'post',
                url: 'status.php',
                data: {
                status: status,
                id: $scope.selectedRowPanel.id
            }
        })
    };

    $scope.answerToUser = function(message) {
       $.getJSON(
            'answeruser.php',
            {
                id: $scope.selectedRowPanel.id,
                emailHombre: $scope.selectedRowPanel.correoHombre,
                emailEmpresa: $scope.selectedRowPanel.email,
                message: message,
                sendDateMessage: moment().format('MM/DD/YYYY hh:mm:ss')
            },
            function(data) {
                $scope.rowsPanelMessage[$scope.rowsPanelMessage.length]= {
                    idsolicitud: $scope.selectedRowPanel.id,
                    emailSend: data,
                    message: message,
                    sendDateMessage: moment().format('MM/DD/YYYY hh:mm:ss')
                };
                $scope.rowsPanelMessageShowing[$scope.rowsPanelMessageShowing.length]= {
                    idsolicitud: $scope.selectedRowPanel.id,
                    emailSend: data,
                    message: message,
                    sendDateMessage: moment().format('MM/DD/YYYY hh:mm:ss')
                };
            }
        );
        $scope.rowsPanelMessageShowing = _.sortBy($scope.rowsPanelMessageShowing, 'sendDateMessage');
        $scope.rowsPanelMessageShowing = darvueltavector($scope.rowsPanelMessageShowing);

        $scope.messageshow = false;
    };
    $scope.sendemail = function() {
        $scope.messageshow = true;
    }
});
