'use strict';
var myApp = angular.module('App', []);
myApp.controller('MainController', function ($scope, $filter) {

    $scope.alertURL = function () {
        alert('Va a acceder a la página web de la marca. ' + $scope.selectedRow.bocata);
    }
    //Panel de usuario:
    //variable con todas las filas cada una esta en el vector donde cada elemento tiene marca y provincia
    var div, table1, table, myData;
    div = document.getElementById("solicitudTable1");
    $scope.rows = JSON.parse(JSON.parse(div.textContent));
    $scope.rows = _.sortBy($scope.rows, 'mark');
    //variables de seleccion de la marca
    $scope.selectedMark = null;
    $scope.selectedProvince = null;
    //obtengo los elementos de los dropdown
    $scope.marks = [];
    $scope.provinces = [];
    for (var l = 0; l < $scope.rows.length; l++) {
        $scope.marks[l] = $scope.rows[l].mark;
        $scope.provinces[l] = $scope.rows[l].province;
    }
    $scope.marks = uniqSort($scope.marks);
    $scope.marksInitial = $scope.marks;
    $scope.provinces = uniqSort($scope.provinces);
    $scope.provincesInitial = $scope.provinces;
    $scope.initialMarks = $scope.marks;
    $scope.initialProvinces = $scope.provinces;
    $scope.showMark = 'Marca';
    $scope.showProvince = 'Provincia';
    //
    $scope.selectMark = true;
    $scope.selectProvince = false;
    //variables del formulario
    $scope.acceptedSend = false;
    $scope.showApplication = false;
    $scope.correoYes = false;
    $scope.showAlert = false;
    $scope.messageAlert = null;
    $scope.noResults = false;
    $scope.ShowNoWarranty = false;
    $scope.showErrorDate = false;
    //
    //variables de las filas de marcas
    $scope.rowsFiltered = $scope.rows;
    $scope.showRows = true;
    $scope.selectedRow = [];
    $scope.markShowing = null;
    $scope.provinceShowing = null;
    //
//var arr=JSON.parse();
//console.log(arr);

    function uniqSort(products) {
        products = _.uniq(products);
        products.sort();
        return products;
    }
    function checkFilterThings() {
        var row, j, row2;
        row = null;
        if ($scope.selectedMark) {
            row = $filter('filter')($scope.rowsFiltered, {'mark': $scope.selectedMark}, false);
            row2 = $filter('filter')($scope.marksInitial, $scope.selectedMark, false);
            if (row[0]) {
                $scope.rowsFiltered = row;
                $scope.marks = row2;
                $scope.provinces = [];
                for (j = 0; j < row.length; j++) {
                    $scope.provinces[j] = row[j].province;
                }
                $scope.provinces = uniqSort($scope.provinces);
                $scope.noResults = false;
            } else {
                $scope.rowsFiltered = row;
                $scope.marks = row2;
                $scope.provinces = [];
                for (j = 0; j < row.length; j++) {
                    $scope.provinces[j] = row[j].province;
                }
                $scope.provinces = uniqSort($scope.provinces);
                $scope.noResults = true;
                $scope.showRows = false;
            }
        } else {
            $scope.marks = $scope.marksInitial;
        }
        if ($scope.selectedProvince) {
            row = $filter('filter')($scope.rowsFiltered, {'province': $scope.selectedProvince}, false);
            row2 = $filter('filter')($scope.provincesInitial, $scope.selectedProvince, false);
            if (row[0]) {
                $scope.rowsFiltered = row;
                $scope.provinces = row2;
                $scope.noResults = false;
            } else {
                $scope.rowsFiltered = row;
                $scope.provinces = row2;
                $scope.noResults = true;
                $scope.showRows = false;
            }
        } else {
            $scope.provinces = $scope.provincesInitial;
        }
    }

    $scope.filterRows = function () {
        $scope.rowsFiltered = $scope.rows;
        $scope.showRows = true;
        $scope.acceptedSend = false;
        $scope.showApplication = false;
        $scope.warrantyShow = false;
        $scope.correoYes = false;
        $scope.showAlert = false;
        $scope.messageAlert = null;
        $scope.noResults = false;
        $scope.ShowNoWarranty = false;
        checkFilterThings();
    };

    $scope.clearAll = function () {
        $scope.rowsFiltered = $scope.rows;
        $scope.selectedMark = "";
        $scope.selectedProvince = "";
        $scope.showRows = true;
        $scope.acceptedSend = false;
        $scope.showApplication = false;
        $scope.warrantyShow = false;
        $scope.correoYes = false;
        $scope.showAlert = false;
        $scope.messageAlert = null;
        $scope.noResults = false;
        $scope.selectMark = true;
        $scope.selectProvince = false;
        $scope.ShowNoWarranty = false;
        checkFilterThings();
    };

    $scope.selectingRow = function (row) {
        for (var i=0;i<$scope.rows2.length;i++) {
            if ($scope.rows2[i].id == row.id) {
                $scope.selectedRow = $scope.rows2[i];
            }
        }
        $scope.showApplication = true;
        if ($scope.selectedRow.email) {
            $scope.correoYes = true;
        }
        $scope.ShowNoWarranty = false;
        $scope.showRows = false;
        $scope.warrantyShow = false;
        $scope.showErrorDate = false;
    };

    $scope.selectingMark = function (value) {
        $.getJSON(
            "marks.php",
            {
                mark: value
            },
            function(data) {
                $scope.rows2 = JSON.parse(data);
            }
        );
        $scope.selectedMark = value;
        $scope.filterRows();
        $scope.showRows = true;
        $scope.warrantyShow = false;
        $scope.selectMark = false;
        $scope.selectProvince = true;
        $scope.ShowNoWarranty = false;
    };

    $scope.selectingProvince = function (value) {
        $scope.selectedProvince = value;
        $scope.filterRows();
        $scope.rowyes = $scope.rowsFiltered;
        $scope.showRows = true;
        $scope.warrantyShow = true;
        $scope.selectMark = false;
        $scope.selectProvince = false;
        $scope.ShowNoWarranty = false;
    };

    $scope.dateEntered = function (buyDate, warranty) {
        if($scope.rows2 == undefined) {
            console.log('waiting');
        } else {
            if (buyDate) {
                if (warranty) {
                    $scope.selectingRow($scope.rowyes[0]);
                } else {
                    $scope.showRows = false;
                    $scope.ShowNoWarranty = true;
                    $scope.warrantyShow = false;
                    $scope.showErrorDate = false;
                }
            } else {
                $scope.showErrorDate = true;
            }
        }
        $scope.buyDateex = buyDate;
    };

    $scope.estaEnGarantia = function () {
        $scope.selectingRow($scope.rowyes[0]);
    };

    $scope.selecting2Mark = function (value) {
        $scope.selectedMark = value;
        $scope.filterRows();
        $scope.showRows = true;
    };

    $scope.selecting2Province = function (value) {
        $scope.selectedProvince = value;
        $scope.filterRows();
        $scope.showRows = true;
    };

    $scope.markselectedfocus = function () {
        setTimeout(function () {
            $('#focusme1').focus();
        }, 0);
    };

    $scope.provinceselectedfocus = function () {
        setTimeout(function () {
            $('#focusme2').focus();
        }, 0);
    };

    $scope.sortClick = function (variable) {
        $scope.rowsFiltered = _.sortBy($scope.rowsFiltered, variable);
    };


    function petitionSQL(sendObject) {
        $.ajax({
            type: 'post',
            url: 'test.php',
            data: {
                mark:  sendObject.mark,
                province: sendObject.province,
                tlf: sendObject.tlf,
                email: sendObject.email,
                web: sendObject.web,
                buyDate: sendObject.buyDate,
                sendDate: sendObject.sendDate,
                serieNumber: sendObject.serieNumber,
                PN: sendObject.PN,
                nombreHombre: sendObject.nombreHombre,
                apellido1Hombre: sendObject.apellido1Hombre,
                apellido2Hombre: sendObject.apellido2Hombre,
                dniHombre: sendObject.dniHombre,
                correoHombre: sendObject.correoHombre,
                conditions: sendObject.conditions,
                comment1: sendObject.comment1,
                comment2: sendObject.comment2,
                telefono: sendObject.telefono,
                shopCenter: sendObject.shopCenter,
                country: sendObject.country,
                provinceMan: sendObject.provinceMan,
                housePlace: sendObject.housePlace,
                fichero: $scope.fichero,
            }
        });
    }


    $scope.applyRepair = function (serieNumber, PN, nombreHombre, apellido1Hombre, apellido2Hombre, dniHombre, correoHombre, conditions, comment1, comment2, telefono, shopCenter, country, provinceMan, housePlace) {
        var sendObject = {
            sendDate: moment().format('MM/DD/YYYY hh:mm:ss'),
            mark: $scope.selectedRow.mark,
            province: $scope.selectedRow.province,
            tlf: $scope.selectedRow.tlf,
            email: $scope.selectedRow.email,
            web: $scope.selectedRow.web,
            buyDate: $scope.buyDateex,
            serieNumber: serieNumber,
            PN: PN,
            nombreHombre: nombreHombre,
            apellido1Hombre: apellido1Hombre,
            apellido2Hombre: apellido2Hombre,
            dniHombre: dniHombre,
            correoHombre: correoHombre,
            conditions: conditions,
            comment1: comment1,
            comment2: comment2,
            telefono: telefono,
            shopCenter: shopCenter,
            country: country,
            provinceMan: provinceMan,
            housePlace: housePlace
        };
        if ($scope.buyDateex) {
            if (serieNumber) {
                if (PN) {
                    if (nombreHombre) {
                        if (apellido1Hombre) {
                            if (apellido2Hombre) {
                                if (dniHombre) {
                                    if (correoHombre) {
                                        if (comment1) {
                                            if (conditions) {
                                                if (telefono) {
                                                    if (country) {
                                                        if (provinceMan) {
                                                            $scope.acceptedSend = true;
                                                            $scope.showApplication = false;
                                                            $scope.correoYes = false;
                                                            $scope.showAlert = false;
                                                            $scope.messageAlert = null;
                                                            $scope.noResults = false;
                                                            petitionSQL(sendObject);
                                                        } else {
                                                            $scope.showAlert = true;
                                                            $scope.messageAlert = 'No ha introducido la provincia';
                                                        }
                                                    } else {
                                                        $scope.showAlert = true;
                                                        $scope.messageAlert = 'No ha introducido el País';
                                                    }
                                                } else {
                                                    $scope.showAlert = true;
                                                    $scope.messageAlert = 'No ha introducido el teléfono';
                                                }
                                            } else {
                                                $scope.showAlert = true;
                                                $scope.messageAlert = 'No ha aceptado las condiciones y términos';
                                            }
                                        } else {
                                            $scope.showAlert = true;
                                            $scope.messageAlert = 'Indique la avería del producto';
                                        }
                                    } else {
                                        $scope.showAlert = true;
                                        $scope.messageAlert = 'Indique su correo';
                                    }
                                } else {
                                    $scope.showAlert = true;
                                    $scope.messageAlert = 'Indique lsu DNI';
                                }
                            } else {
                                $scope.showAlert = true;
                                $scope.messageAlert = 'Indique su segundo apellido';
                            }
                        } else {
                            $scope.showAlert = true;
                            $scope.messageAlert = 'Indique su primer apellido';
                        }
                    } else {
                        $scope.showAlert = true;
                        $scope.messageAlert = 'Indique su nombre';
                    }
                } else {
                    $scope.showAlert = true;
                    $scope.messageAlert = 'Indique el numero de producto';
                }
            } else {
                $scope.showAlert = true;
                $scope.messageAlert = 'Indique el numero de serie';
            }
        } else {
            $scope.showAlert = true;
            $scope.messageAlert = 'Indique la fecha de compra';
        }
        $scope.buyDate = null;
        $scope.serieNumber = null;
        $scope.PN = null;
        $scope.nombreHombre = null;
        $scope.apellido1Hombre = null;
        $scope.apellido2Hombre = null;
        $scope.nacimientoHombre = null;
        $scope.dniHombre = null;
        $scope.correoHombre = null;
    };
    $scope.fichero = "";
    $scope.submitForm = function () {
        var fd = new FormData(document.getElementById("fileinfo"));
        $.ajax({
            url: "upload.php",
            type: "POST",
            data: fd,
            enctype: 'multipart/form-data',
            processData: false,  // tell jQuery not to process the data
            contentType: false   // tell jQuery not to set contentType
        }).done(function (data) {
            $scope.fichero = data.split('<')[0];
            $scope.fichero = $scope.fichero.substring(8, $scope.fichero.length);
        });
        return false;
    };
});
